What is an alternative term for pinging?
Answer: echo.

What is the term used when a server cannot be pinged?
Answer: server timeout.

A security setting that limits the communication of systems from outside the server.
Answer:firewall.

A security setting that limits the communication of systems from within the server.
Answer: firewall rules.

From which tab in the instance information summary can we change the firewall rules of the instance?
Answer:security.

ICMP stands for?
Answer:Internet Control Message Protocol.

TCP stands for?

Answer:Transmission Control Protocol.
Which protocol does a ping request use?

Answer:Internet Control Message Protocol.
What is the IP address that indicates access from anywhere?

Answer:0.0.0.0/0
A command that can be used to look on various usage metrics for a given server.
Answer:htop.


Processor or memory __________ means the amount of resources needed to keep a given task or process running smoothly within the operating system.
Answer:Utilization 

A command that can be used to determine disk storage usage.
Answer: df

From which tab in the instance information summary can we view the usage of an instance without directly accessing the instance?
Answer: Cloudwatch

A command that can be used to edit the contents of a file.
Answer: nano

A command used to host a Node.js app.
Answer: node app.js

Which protocol is used to enable access to a Node.js app on a given port?
Answer:Transmission Control Protocol (TCP)